




class Account
  def method_missing(name, *args)
    if name.to_s.start_with?('log')
      "#{name} method is invoked with args: #{args.inspect}"
    else
      super
    end
  end
end

Account.new.log
=> "log method is invoked with args: []"

Account.new.log_item('[Critical] Issue with user', 2)
=> "log_item method is invoked with args: [\"[Critical] Issue with user\", 2]"

Account.new.respond_to?(:log_item)
=> false

Account.find_by(amount: 10)
Account.find_by(email:'bohdan@viter.in')

class Account
  def respond_to_missing?(name, include_private = false)
    name.to_s.start_with?('log') || super
  end
end

Account.new.respond_to?(:log_item)
=> true