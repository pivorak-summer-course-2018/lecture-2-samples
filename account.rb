# frozen_string_literal: true

# Class, that will handle our accounts logic
class Account
  @@active_accounts = 0
  attr_reader :account_number, :balance, :currency

  def initialize(options = {})
    @account_number = options.fetch(:account_number)
    @balance = options.fetch(:balance, 0)
    @balance_limit = options.fetch(:balance_limit)
    @currency = options.fetch(:currency, :usd)
    @@active_accounts += 1 # @@active_accounts + 1
  end

  def sub_funds(amount)
    @balance -= amount # @balance - amount
  end

  def add_funds(amount)
    @balance += amount # @balance + amount
  end
end
