
module Banking
  class Account
    include BalanсeOperations

    attr_reader :account_number, :balance, :currency

    def initialize(options = {})
      @account_number = options.fetch(:account_number)
      @balance = options.fetch(:balance, 0)
      @balance_limit = options.fetch(:balance_limit)
      @currency = options.fetch(:currency, :usd)
    end

    protected

    def this_is_protected_method; end

    private

    def this_is_private_method; end
  end


Banking::Account.new(options).this_is_protected_method
NoMethodError: protected method `this_is_protected_method`
called for #<Banking::Account:0x007ffa5b209fb0>

Banking::Account.new(options).this_is_private_method
NoMethodError: private method `this_is_private_method`
called for #<Banking::Account:0x007ffa5b188e10>

Banking::Account.new(options).send(:this_is_protected_method)
=> nil

